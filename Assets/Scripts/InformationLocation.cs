﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InformationLocation : MonoBehaviour
{
    public Vector3 Location;
    public float CameraZoomSize;

    void Start()
    {
        Location = transform.position;
        Location.z = -10;
    }
}
