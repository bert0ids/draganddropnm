﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OpenKeyboard : Core.Utilities.Singleton<OpenKeyboard>
{
    public Animator keyboardAnim;
    public bool open = false;
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            ToggleKeyboard();
        }
    }
    public void ToggleKeyboard()
    {
        open = !open;
        keyboardAnim.SetBool("Open", open);
    }


}
