﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LocationInformationDone : MonoBehaviour
{
    public GameObject PlayButton;
    public List<DiscoverLocationButton> DiscoverButtons = new List<DiscoverLocationButton>();

    private void Start()
    {
        PlayButton.SetActive(false);
    }

    private void Update()
    {
        if (CheckIfAllDone())
            PlayButton.SetActive(true);
    }


    public bool CheckIfAllDone()
    {
        int count = 0;
        foreach (DiscoverLocationButton discoverButton in DiscoverButtons)
        {
            if (discoverButton.IsDone)
                count++;
        }
        if (count >= DiscoverButtons.Count)
            return true;

        return false;
    }
}
