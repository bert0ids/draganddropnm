﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DiscoverIcon : MonoBehaviour
{
    public GameObject InformationPanelPrefab;
    public GameObject MaskedImagePrefab;

    public string Name;
    public string Description;
    public List<Sprite> Images = new List<Sprite>();

    // Use this for initialization
    void Start()
    {

    }

    public void SpawnInfoPanel()
    {
        DiscoverInformationPanel infoPanelTemp = Instantiate(InformationPanelPrefab, GameObject.Find("MainCanvas").transform).GetComponent<DiscoverInformationPanel>();
        infoPanelTemp.NameText.text = Name;
        infoPanelTemp.DescriptionText.text = Description;
        foreach (Sprite sprite in Images)
        {
            Image temp = Instantiate(MaskedImagePrefab, infoPanelTemp.ImagesParent).GetComponent<MaskedImage>().MainImage;
            temp.sprite = sprite;
        }
    }
}
