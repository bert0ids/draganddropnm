﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// disable by listening to anim event
/// </summary>
public class DisablerByAnim : MonoBehaviour
{

    public void Disable()
    {
        gameObject.SetActive(false);
    }
}
