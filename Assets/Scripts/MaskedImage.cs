﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MaskedImage : MonoBehaviour
{
    public UnityEngine.UI.Image MainImage;
    public GameObject NextPrevButtons;
    public GameObject ZoomedImagePrefab;
    public Transform ZoomImagesParent;

    public void ZoomedImage()
    {
        EventManager.TriggerEvent("ShowButtons");
        ZoomImagesParent = GameObject.Find("ImageZoomParent").transform;
        DiscoverZoomedImage zoomedImage = Instantiate(ZoomedImagePrefab, ZoomImagesParent).GetComponent<DiscoverZoomedImage>();
        ZoomImagesParent.GetComponentInParent<DiscoverInformationPanel>().CurrentZoomedImage = zoomedImage.gameObject;
        zoomedImage.ZoomedImage.sprite = MainImage.sprite;
    }
}
