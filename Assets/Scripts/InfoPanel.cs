﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InfoPanel : MonoBehaviour
{
    public Text ScientificNameText;
    public Image EntityImage;
    public QueueManager[] manager;
    public GameObject ContinueButton;

    public void ActivateButton()
    {
        ContinueButton.SetActive(true);
    }

    public void DeActivateButton()
    {
        ContinueButton.SetActive(false);
    }

    public void Exit()
    {
        GetComponent<Animator>().SetBool("Open", false);
        if (manager[0].Draggables.Count <= 0 && manager[1].Draggables.Count <= 0)
        {
            GameEnd();
            return;
        }
        Timer.instance.ToggleTimer();
       
    }
    public void GameEnd()
    {
        Timer.instance.StopTime();
    }
    public void Deactivate  ()
    {       
        gameObject.SetActive(false);
    }
    private void OnEnable()
    {
        this.AddEventListenerGlobal<CorrectDrag>(CorrectDragCallback);
    }
    private void OnDisable()
    {
        this.RemoveEventListenerGlobal<CorrectDrag>(CorrectDragCallback);
    }
    public void CorrectDragCallback(CorrectDrag obj)
    {
        EntityImage.sprite = obj.ImageSprite;
        ScientificNameText.text = "<i>" + obj.Name + "</i>" + "  is located in " + 
            (obj.Location.Equals(MapLocation.LuzonPAIC) ? "Luzon PAIC" :
            obj.Location.Equals(MapLocation.MindanaoPAIC) ? "Mindanao PAIC" :
            obj.Location.Equals(MapLocation.Mindoro) ? "Mindoro" : 
            obj.Location.Equals(MapLocation.NegrosPanayPAIC) ? "Negros-Panay PAIC" :
            obj.Location.Equals(MapLocation.PalawanPAIC) ? "Palawan PAIC" : 
            obj.Location.Equals(MapLocation.Sibuyan) ? "Sibuyan" :
            string.Empty);
        //find the name to the inventory then set the ui
    }
}
