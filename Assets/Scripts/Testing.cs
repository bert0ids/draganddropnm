﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using UnityEngine.Events;
[System.Serializable]

public class Testing : MonoBehaviour
{
    public float Time;
    public string Name ="Default";
    public UserHolder uh;
    public void a()
    {
        uh.GameEndedCallback(Time);
    }
    public void b()
    {
        uh.AssignNameToUser(Name);
    }
    private void Start()
    {
        uh = GetComponent<UserHolder>();
    }

}
