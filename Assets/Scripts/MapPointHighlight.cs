﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapPointHighlight : MonoBehaviour
{
    MapPoint mapPoint;

    private void Start()
    {
        mapPoint = GetComponentInParent<MapPoint>();
    }

    public void Hide()
    {
        mapPoint.HighlightMapObject.SetActive(false);
        mapPoint.HighlightImageObject.SetActive(false);
    }

    public void Highlight()
    {
        mapPoint.HighlightMapObject.SetActive(true);
        mapPoint.HighlightImageObject.SetActive(true);
    }
}
