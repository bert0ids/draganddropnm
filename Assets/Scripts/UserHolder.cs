﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using UnityEngine.Events;



[System.Serializable]
public class ScoreEvent : UnityEvent<float,int>
{

}
public class UserHolder : Core.Utilities.Singleton<UserHolder>
{

    User CurrentUser = null;
    int rank;

    [Header("Events")]
    public ScoreEvent highScoreEvent;
    public ScoreEvent top10Event;
    public ScoreEvent notSpecialEvent;

    public void AssignNameToUser(string name)
    {
        if (name == "")
        {
            Debug.Log("please fill out the name");
            return;
        }
        if (CurrentUser == null)
            return;

        CurrentUser.Name = name;
        LeaderBoard.instance.InsertAt(rank, CurrentUser);

        CurrentUser = null;
    }
    public int FindPlacement(float Time, List<User> u)
    {
        if (u.Count == 0)
            return 0;

        for (int i = 0; i < u.Count; i++)
        {
            if (Time < u[i].Time)
            {
                return i;
            }
        }
        return u.Count;
    }
    public void GameEndedCallback(float time)
    {
        CurrentUser = new User();
        CurrentUser.Time = time;
        rank = FindPlacement(time, LeaderBoard.instance.HighScores);

        if (rank == 0)
            if (highScoreEvent != null)
            {
                highScoreEvent.Invoke(time,rank);
                return;
            }
        if (rank < LeaderBoard.instance.Limit)
            if (top10Event != null)
            {
                top10Event.Invoke(time,rank);
                return;
            }
        if (notSpecialEvent != null)
            notSpecialEvent.Invoke(time,rank);
    }


}



