﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChildShuffler : MonoBehaviour
{
    public void ShuffleChildren()
    {
        List<GameObject> temp = new List<GameObject>();
        foreach (Transform child in transform)
        {
            temp.Add(child.gameObject);
        }
        temp.ForEach(o => o.transform.SetParent(null, false));
        while (temp.Count > 0)
        {
            int randomIndex = Random.Range(0, temp.Count);
            temp[randomIndex].transform.SetParent(transform, false);
            temp.RemoveAt(randomIndex);
            temp.TrimExcess();
        }

    }
}
