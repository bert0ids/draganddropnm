﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DiscoverZoomedImage : MonoBehaviour
{
    public Image ZoomedImage;

    public void DeletePanel()
    {
        EventManager.TriggerEvent("HideButtons");
        Destroy(gameObject);
    }
}
