﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QueueManager : MonoBehaviour
{
    public float VerticalSpacing;
    public GameObject DraggableObjectsParent;
    public GameObject QueueParent;
    public GameObject InformationPanel;
    public GameObject InCorrectIndicator;
    public GameObject Overlay;
    public int ObjectCountPerQueue;

    // [HideInInspector]
    public List<DragObject> Draggables = new List<DragObject>();

    float uniformY;
    // Use this for initialization
    void Start()
    {
        uniformY = transform.position.y;
        Draggables.Clear();
        Draggables.TrimExcess();
        //AssignFromParent();
        //Draggables.ForEach(o => o.Manager = this);
        GetFromPool(5);
        AlignObjects();
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void GetFromPool(int count)
    {
        QueueParent.GetComponent<ChildShuffler>().ShuffleChildren();
        for (int i = 0; i < count; i++)
        {
            if (QueueParent.transform.GetChild(i).GetComponent<DragObject>())
                Draggables.Add(QueueParent.transform.GetChild(i).GetComponent<DragObject>());
        }
        Draggables.ForEach(o => o.Manager = this);
        Draggables.ForEach(o => o.transform.SetParent(DraggableObjectsParent.transform));
    }

    public void RePopulateFromQueue()
    {
        for (int i = 0; i < ObjectCountPerQueue; i++)
        {
            if (i > QueueParent.transform.childCount - 1)
                break;

            if (QueueParent.transform.GetChild(i).GetComponent<DragObject>())
                Draggables.Add(QueueParent.transform.GetChild(i).GetComponent<DragObject>());
        }
        Draggables.ForEach(o => o.Manager = this);
        Draggables.ForEach(o => o.transform.SetParent(DraggableObjectsParent.transform));
    }

    public void AssignFromParent()
    {
        foreach (Transform child in DraggableObjectsParent.transform)
            Draggables.Add(child.GetComponent<DragObject>());
    }

    public void AlignObjects()
    {
        float separator = 0;
        float uniformColumn = transform.position.x;
        foreach (DragObject dragObj in Draggables)
        {
            dragObj.transform.position = new Vector3(uniformColumn, uniformY + separator, dragObj.transform.position.z);
            separator += VerticalSpacing;
        }
        Draggables.ForEach(o => o.AssignNewOriginPoint(o.transform.position));
    }

    public void ShowPanel()
    {

        InformationPanel.SetActive(true);
        InformationPanel.GetComponent<Animator>().SetBool("Open", true);
        Overlay.SetActive(true);
    }

    public void UpdateOrigins()
    {
        Draggables.TrimExcess();
        float separator = 0;
        float uniformColumn = transform.position.x;
        foreach (DragObject dragObj in Draggables)
        {
            dragObj.AssignNewOriginPoint(new Vector3(uniformColumn, uniformY + separator, dragObj.transform.position.z));
            separator += VerticalSpacing;
        }
        if (Draggables.Count <= 0 && QueueParent.transform.childCount > 0)
        {
            RePopulateFromQueue();
            UpdateOrigins();
        }
    }
}
