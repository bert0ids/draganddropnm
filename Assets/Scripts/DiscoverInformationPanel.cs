﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DiscoverInformationPanel : MonoBehaviour
{
    public Text NameText;
    public Text DescriptionText;
    public Transform ImagesParent;
    public GameObject NextPrevButtons;
 
    public GameObject CurrentZoomedImage;

    // Use this for initialization
    void Start()
    {
        EventManager.StartListening("ClosePanel", ClosePanel);
        EventManager.StartListening("ShowButtons", ShowButtons);
        EventManager.StartListening("HideButtons", HideButtons);
    }

    public void ShowButtons()
    {
        NextPrevButtons.SetActive(true);
    }

    public void HideButtons()
    {
        NextPrevButtons.SetActive(false);
    }

    public void ClosePanel()
    {
        Destroy(gameObject);
        Transform[] trs = GameObject.Find("MainCanvas").GetComponentsInChildren<Transform>(true);
        foreach (Transform t in trs)
        {
            if (t.name == "NextPrev")
            {
                t.gameObject.SetActive(true);
            }
        }
    }

    public void NextImage()
    {
        for (int i = 0; i < ImagesParent.childCount; i++)
        {
            if (ImagesParent.GetChild(i).GetComponent<MaskedImage>().MainImage.sprite.Equals(CurrentZoomedImage.GetComponent<DiscoverZoomedImage>().ZoomedImage.sprite))
            {
                if (i >= ImagesParent.childCount - 1)
                {
                    CurrentZoomedImage.GetComponent<DiscoverZoomedImage>().DeletePanel();
                    ImagesParent.GetChild(0).GetComponent<MaskedImage>().ZoomedImage();
                }
                else
                {
                    CurrentZoomedImage.GetComponent<DiscoverZoomedImage>().DeletePanel();
                    ImagesParent.GetChild(i + 1).GetComponent<MaskedImage>().ZoomedImage();
                    break;
                }
            }
        }

    }

    public void PreviousImage()
    {
        for (int i = 0; i < ImagesParent.childCount; i++)
        {
            if (ImagesParent.GetChild(i).GetComponent<MaskedImage>().MainImage.sprite.Equals(CurrentZoomedImage.GetComponent<DiscoverZoomedImage>().ZoomedImage.sprite))
            {
                if (i == 0)
                {
                    CurrentZoomedImage.GetComponent<DiscoverZoomedImage>().DeletePanel();
                    ImagesParent.GetChild(ImagesParent.childCount - 1).GetComponent<MaskedImage>().ZoomedImage();
                    break;
                }
                else
                {
                    CurrentZoomedImage.GetComponent<DiscoverZoomedImage>().DeletePanel();
                    ImagesParent.GetChild(i - 1).GetComponent<MaskedImage>().ZoomedImage();
                }
            }
        }
    }
}
