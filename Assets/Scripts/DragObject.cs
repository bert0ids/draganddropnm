﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class CorrectDrag : GameEvent
{
    public string Name;
    public MapLocation Location;
    public Sprite ImageSprite;
}


public class DragObject : MonoBehaviour
{
    public UnityEngine.UI.Image ThumbIcon;
    public float FollowRate;
    public MapLocation CorrectLocation;
    [HideInInspector]
    public bool OnCorrectPoint;
    [HideInInspector]
    public QueueManager Manager;

    Vector3 origin;
    bool active;
    // Use this for initialization
    void Start()
    {
        OnCorrectPoint = false;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (active)
        {
            Vector3 targetPosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            targetPosition.z = -5;
            transform.position = Vector3.Lerp(transform.position, targetPosition, Time.deltaTime * FollowRate);
        }
        else
        {
            transform.position = Vector3.Lerp(transform.position, origin, Time.deltaTime * FollowRate);
        }
    }

    public void OnMouseClick()
    {
        active = true;
    }

    public void OnMouseRelease()
    {
        active = false;
        if (OnCorrectPoint)
        {
            Timer.instance.ToggleTimer();
            //go activate here 
            Manager.Draggables.RemoveAt(Manager.Draggables.FindIndex(o => o.Equals(this)));
            Destroy(gameObject);
            Manager.ShowPanel();
            Manager.UpdateOrigins();
            CorrectDrag cd = new CorrectDrag();
            cd.Name = this.name;
            cd.Location = this.CorrectLocation;
            cd.ImageSprite = this.ThumbIcon.sprite;
            this.RaiseEventGlobal<CorrectDrag>(cd);
        }
        else
        {
            Manager.InCorrectIndicator.SetActive(false);
            Manager.InCorrectIndicator.SetActive(true);
            Manager.InCorrectIndicator.GetComponent<Animator>().SetTrigger("Open");


        }
    }
    IEnumerator Disable(GameObject g)
    {
        yield return new WaitForSeconds(1);
        g.SetActive(false);
    }

    public void OnMouseHover()
    {

    }

    public void OnMouseOut()
    {

    }

    public void AssignNewOriginPoint(Vector3 newPosition)
    {
        origin = newPosition;
    }
}
