﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
[System.Serializable]
public class TimeStopped : UnityEvent<float> {}
[System.Serializable]
public class TimePaused : UnityEvent {}


public class Timer : Core.Utilities.Singleton<Timer> {
    // public delegate void TimeStopped(float time);
    // public event TimeStopped timeEvent;
    public string time { get { return TimeConverter.TimeConvert(CurrentTime); } }
    float CurrentTime = 0;
    [SerializeField]
    bool Paused = false;

    public TimeStopped timeEvent;
    public TimePaused pausedEvent;

    public void StopTime()
    {
        Paused = true;
        if(timeEvent != null)
        {
            timeEvent.Invoke(CurrentTime);
            CurrentTime = 0;
        }
    }
    public void ToggleTimer()
    {
        Paused = !Paused;
        if(Paused == true)
        {
            if (pausedEvent != null)
                pausedEvent.Invoke();
        }
    }
	
	// Update is called once per frame
	void Update () {
		if(!Paused)
        {
            CurrentTime += Time.deltaTime;
        }
	}
}
