﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class LeaderBoard : Core.Utilities.Singleton<LeaderBoard>
{
    string FileName = "/HighScores.dat";
    public int Limit = 10;
    public List<User> HighScores;
    protected override void Awake()
    {
        base.Awake();
        Init();
    }
    public void InsertAt(int index, User u)
    {
        HighScores.Insert(index, u);

        if (HighScores.Count > 10)
        {
            HighScores.RemoveAt(Limit);
        }
    }

    private void OnEnable()
    {
        List<User> u = new List<User>();
        u = this.LoadArray<User>(FileName).ToList();
        if (u.Count == 0)
            return;
        HighScores = u;
    }
    private void OnDisable()
    {
        this.SaveArray<User>(HighScores.ToArray(), FileName);
    }
    public void Init()
    {
        HighScores = new List<User>();
        for (int i = 0; i < Limit; i++)
        {

            HighScores.Add(new User());
        }
    }
    private void Update()
    {
       // if (Input.GetKeyDown(KeyCode.Space))
         //   Init();
    }
}
