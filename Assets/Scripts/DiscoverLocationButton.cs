﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DiscoverLocationButton : MonoBehaviour
{
    [HideInInspector]
    public bool IsDone;
    Button button;

    private void Start()
    {
        IsDone = false;
        button = GetComponent<Button>();
        button.image.color = Color.white;
    }

    public void MarkAsFinished()
    {
        IsDone = true;
        button.image.color = new Color(0.65f,1f,0.65f);
    }
}
