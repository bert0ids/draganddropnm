﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum MapLocation
{
    LuzonPAIC,
    Mindoro,
    Sibuyan,
    NegrosPanayPAIC,
    PalawanPAIC,
    MindanaoPAIC
}

public class MapPoint : MonoBehaviour
{
    public MapLocation Location;
    public GameObject HighlightMapObject;
    public GameObject HighlightImageObject;
    Animator animator;
    private void Start()
    {
       animator = GetComponentInChildren<Animator>(); 
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.GetComponent<DragObject>())
        {
            animator.SetBool("Hovering", true);
            if (other.GetComponent<DragObject>().CorrectLocation.Equals(Location))
                other.GetComponent<DragObject>().OnCorrectPoint = true;

        }
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        if (other.GetComponent<DragObject>())
        {
            animator.SetBool("Hovering", false);
            if (other.GetComponent<DragObject>().CorrectLocation.Equals(Location))
                other.GetComponent<DragObject>().OnCorrectPoint = false;
        }

    }
}
