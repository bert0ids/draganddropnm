﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneLoader : MonoBehaviour {
    public float DelayTime = 1;
    public void LoadScene(string scene)
    {
        SceneManager.LoadScene(scene);
    }
    public void LoadScene(int scene)
    {
        SceneManager.LoadScene(scene);
    }
    public void LoadWithDelay(string scene)
    {
        StartCoroutine(delay(DelayTime, scene));
    }
    public void LoadWithDelay(int scene)
    {
        StartCoroutine(delay(DelayTime, scene));
    }
    IEnumerator delay(float d,string name)
    {
        yield return new WaitForSeconds(d);
        SceneManager.LoadScene(name);

    }
    IEnumerator delay(float d, int name)
    {
        yield return new WaitForSeconds(d);
        SceneManager.LoadScene(name);

    }

}
