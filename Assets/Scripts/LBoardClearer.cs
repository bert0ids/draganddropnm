﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LBoardClearer : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if ((Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift)) && Input.GetKeyDown(KeyCode.C))
        {
           
            LeaderBoard.instance.Init();
            gameObject.SendMessage("Assign");
        }

    }
}
