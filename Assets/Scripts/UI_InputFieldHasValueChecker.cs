﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_InputFieldHasValueChecker : MonoBehaviour {
    public InputField inputF;
    public Button saveBtn;
    private void Awake()
    {
        inputF = GetComponent<InputField>();
        inputF.characterLimit = 3;

    }
    private void OnEnable()
    {
        inputF.onValueChanged.AddListener(delegate { InputFieldCallback(); });
    }
    private void OnDisable()
    {
        inputF.onValueChanged.RemoveListener(delegate { InputFieldCallback(); });
    }
    void InputFieldCallback()
    {
        if (inputF.text.Length >= 3)
            inputF.interactable = false;

        if (inputF.text == "")
        {
            saveBtn.interactable = false;
        }
        else
            saveBtn.interactable = true;
    }
}
