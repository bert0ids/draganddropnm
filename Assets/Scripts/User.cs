﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[System.Serializable]
public class User  {
    public string Name;
    public string UniqueID;
    public float Time;

    public User()
    {
        Name = "Blank";
        Time = Mathf.Infinity;
        UniqueID = Guid.NewGuid().ToString();
    }
    public User(string name,string id,float time)
    {
        UniqueID = Guid.NewGuid().ToString();
        Name = name;
        UniqueID = id;
        Time = time;
    }
}
