﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public static class TimeConverter{
    public static string TimeConvert(float t)
    {
        TimeSpan T = TimeSpan.FromSeconds(t);
        string answer = string.Format("{0:D2}:{1:D2}:{2:D2}:{3:D3}",
                T.Hours,
                T.Minutes,
                T.Seconds,
                T.Milliseconds);
        return answer;
    }
}
