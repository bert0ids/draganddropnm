﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
namespace Test.VKeyboard
{

    [System.Serializable]
    public class KeyboardLetterEvent : UnityEvent<string>
    {

    }
    public class VirtualKeyboard : MonoBehaviour
    {

        [Header("KeyboardEvents")]
        public KeyboardLetterEvent keyEvent;

        public void RaisekeyEvent(string letter)
        {
            if (keyEvent != null)
                keyEvent.Invoke(letter);
        
        }
    }
}
