﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_LeaderBoardAssignValues : MonoBehaviour {
    public Transform[] Objects;

	// Use this for initialization
	void Start () {

        Assign();
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
    public void Assign()
    {
        Debug.Log("Reassigned");
        for(int i = 0; i < Objects.Length; i++) {
            Text[] t = Objects[i].GetComponentsInChildren<Text>();
            if (LeaderBoard.instance.HighScores[i].Name == "Blank")
            {
                t[0].text = "--";
                t[1].text = "--";

            }
            else
            {
                t[0].text = TimeConverter.TimeConvert(LeaderBoard.instance.HighScores[i].Time);
                t[1].text = LeaderBoard.instance.HighScores[i].Name;
            }
        }
    }
}
