﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[System.Serializable]
public class Plants
{
    public string Name;
    public Texture2D Texture;
    public string Description;

    #region constructors
    public Plants()
    {
        Name = "BLANK";
        Texture = null;
        Description = "default";
    }
    public Plants(string name, Texture2D texture,string description)
    {
        Name = name;
        Texture = texture;
        Description = description;

    }
    #endregion

}
[CreateAssetMenu(fileName ="Inventory",menuName ="Inventory/Plants")]
public class PlantsInventory : InventoryHolder<Plants> {
}
