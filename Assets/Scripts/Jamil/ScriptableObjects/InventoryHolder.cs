﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// scriptable object,non savable at runtime that holds an inventory
/// </summary>
public class InventoryHolder<T> : ScriptableObject {
    public List<T> Inventory;
}
