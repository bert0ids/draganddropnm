﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
namespace Test.VKeyboard
{
    [System.Serializable]
    public class HitEnterEvent : UnityEvent
    { }




    public class KeyboardTextReciever : MonoBehaviour
    {
        public string[] prohibitedWords;
        public string Word;
        public string value { get { return RealValue.text; } }
        public int WordLengthLimit = 3;
        [SerializeField]
        Text PlaceHolder,RealValue;
        Animator textAnim;
        public HitEnterEvent enterEvent;
        private void Awake()
        {
            //PlaceHolder = GetComponentInChildren<Text>();
            textAnim = PlaceHolder.GetComponent<Animator>();
        }
        public void VKeyboardCallback(string letter)
        {
            
            //Debug.Log(letter);
           
            switch (letter)
            {
                case "BACKSPACE":

                    
                    if (Word.Length <= 0)
                    {
                        //AssignWordToText("Enter 3 Letter Initials");
                        return;

                    }
                    Word = Word.Remove(Word.Length - 1);
                    if (Word.Length <= 0)
                    {
                        PlaceHolder.gameObject.SetActive(true);
                        AssignWordToText(Word);
                        return;

                    }

                    AssignWordToText(Word);
                    break;

                case "ENTER":
            
                    if (CheckWordProhibited(Word))
                    {
                        AssignWordToText("Use of unapropriate words are prohibited");
                        Word = "";

                    }
                    else if (Word == "")
                    {

                    }
                    else
                    {
                        Submit();
                    }

                    break;
                default:
                    if (Word.Length < WordLengthLimit)
                    {
                        PlaceHolder.gameObject.SetActive(false);
                        Word = Word + letter;
                        AssignWordToText(Word);
                    }
                    break;
            }

            textAnim.SetTrigger("Trigger");

        }
        public void AssignWordToText(string word)
        {
            RealValue.text = word;
        }
        public void Submit()
        {
            UserHolder.instance.AssignNameToUser(Word);
            if (enterEvent != null)
                enterEvent.Invoke();
           
        }
        public bool CheckWordProhibited(string w)
        {
            foreach(string s in prohibitedWords)
            {
                if (w == s)
                    return true;
                
            }
            return false;
        }
    }
}
