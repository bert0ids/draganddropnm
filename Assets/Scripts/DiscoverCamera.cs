﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DiscoverCamera : MonoBehaviour
{
    public Camera SceneCamera;
    public float MoveRate;
    public float ZoomRate;
    public List<InformationLocation> CameraLocations = new List<InformationLocation>();
    public List<GameObject> MapHighlight = new List<GameObject>();
    public Text NamePlaceHolder;
    public List<string> NameLocations;
    Vector3 defaultPosition;
    float defaultSize;
    InformationLocation currentLocation;

	/// <summary>
	/// Additional Asthetic by Joash Bulan
	/// </summary>
	#region 
	public Animator Selection; //Pins
	public List<string> AnimationName;
	#endregion

    // Use this for initialization
    void Start()
    {
        defaultPosition = SceneCamera.transform.position;
        defaultSize = SceneCamera.orthographicSize;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (currentLocation)
        {
            SceneCamera.transform.position = Vector3.Lerp(SceneCamera.transform.position, currentLocation.Location, MoveRate * Time.deltaTime);
            SceneCamera.orthographicSize = Mathf.Lerp(SceneCamera.orthographicSize, currentLocation.CameraZoomSize, ZoomRate * Time.deltaTime);
        }
        else
        {
            SceneCamera.transform.position = Vector3.Lerp(SceneCamera.transform.position, defaultPosition, MoveRate * Time.deltaTime);
            SceneCamera.orthographicSize = Mathf.Lerp(SceneCamera.orthographicSize, defaultSize, ZoomRate * Time.deltaTime);
        }
    }

    public void RemoveCurrentLocation()
    {
        currentLocation = null;
        MapHighlight.ForEach(o => o.SetActive(true));
        Selection.Play("InitialState", 0);
    }

    public void AssignFirst()
    {
		if (CameraLocations.Count > 0)
		{
			currentLocation = CameraLocations [0];
			Selection.Play(AnimationName[0], 0);
		}
    }

    public void ZoomToLocation(int index)
    {
        currentLocation = CameraLocations[index];
        HighlightLocation(index);
		Selection.Play(AnimationName[index], 0);
    }

    public void HighlightLocation(int index)
    {
        MapHighlight.ForEach(o => o.SetActive(false));
        if (index < MapHighlight.Count)
        {
            MapHighlight[index].SetActive(true);
            NamePlaceHolder.text = NameLocations[index];
        }
        else
        {
            MapHighlight.ForEach(o => o.SetActive(false));
            NamePlaceHolder.text = "";
        }
    }
}
