﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class UI_ScoreShow : MonoBehaviour
{
    //public InputField inputF;
    public Text[] texts;
    public Button Continue;
    public OpenKeyboard Keyboard;
    private void Awake()
    {
      
    }
    public void HigshCoreCallback(float t, int r)
    {
        Keyboard.ToggleKeyboard();
        //inputF.gameObject.SetActive(true);
        texts[0].text = "CONGRATULATIONS, NEW HIGH SCORE!";
        texts[1].text = "<color=#837A00FF>Your time:</color> " + TimeConverter.TimeConvert(t);
        texts[2].text = "You are top: " + (r+1).ToString();
    }
    public void topTenCallback(float t, int r)
    {
        Keyboard.ToggleKeyboard();
        //inputF.gameObject.SetActive(true);
        texts[0].text = "CONGRATULATIONS!";
        texts[1].text = "<color=#837A00FF>Your time:</color> " + TimeConverter.TimeConvert(t);
        texts[2].text = "You are top: " + (r+1).ToString();
    }
    public void NotSpecialCallback(float t, int r)
    {
        Continue.gameObject.SetActive(true);
       // inputF.gameObject.SetActive(false);
        texts[0].text = "Good Job!";
        texts[1].text = "<color=#837A00FF>Your time:</color> " + TimeConverter.TimeConvert(t);
        texts[2].text = "Unfortunately, your time is not enough to make it to the top 10";
    }
}
