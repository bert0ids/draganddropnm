﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_InputFieldValueGetter : MonoBehaviour {
    public InputField inputF;
    public Button btn;
    string Name;
    private void Awake()
    {
        btn = GetComponent<Button>();
    }
    private void OnEnable()
    {
        btn.onClick.AddListener(delegate { ButtonCallback(); });

    }
    private void OnDisable()
    {
        btn.onClick.RemoveListener(delegate { ButtonCallback(); });
    }
    public void ButtonCallback()
    {
        Name = inputF.text;
        UserHolder.instance.AssignNameToUser(Name);
        Name = "";
    }
    
}
